##################################################

##################################################

ProjectName = mlp

CFLAGS	= -Wall -std=c++11
LDFLAGS	=

PKG = gtkmm-3.0

##################################################

CC	= g++
LINKER	= g++

##################################################

CFLAGS	+= `pkg-config ${PKG} --cflags`
LDFLAGS	+= `pkg-config ${PKG} --libs`

##################################################

SourcePath	= sources
ObjectsPath	= sources/objects

# Collect source files
CPP	= main.cpp pessoa.cpp aluno.cpp catp.cpp window.cpp manager.cpp

# Each .cpp file produces a .o counterpart
SRC	= $(patsubst %, $(SourcePath)/%, $(CPP))
OBJ	= $(patsubst $(SourcePath)/%.cpp, $(ObjectsPath)/%.o, $(SRC))

##################################################

.PHONY: all prepare test clean

all: objdir $(ProjectName)

objdir:
	if [ ! -d $(ObjectsPath) ]; then mkdir -p $(ObjectsPath); fi

$(ProjectName): $(OBJ)
	$(CC) -o $@ $^ $(LDFLAGS)

$(ObjectsPath)/%.o: $(SourcePath)/%.cpp
	$(CC) -c $(CFLAGS) -o $@ $^

##################################################

prepare:
	mkdir $(SourcePath) $(ObjectsPath)

test:
	./$(ProjectName)

clean:
	rm -rf $(ObjectsPath) $(ProjectName)

##################################################
