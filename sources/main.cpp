#include <vector>
#include <typeinfo>

#include <gtkmm.h>

#include "aluno.hpp"
#include "catp.hpp"
#include "window.hpp"

using namespace std;

int main(int argc, char *argv[]){
	auto app =
		Gtk::Application::create(argc, argv, "org.gtkmm.examples.base");

	Window w;
	return app->run(*(w.get_window()));
}
