#pragma once

#include <vector>
#include <string>
#include "aluno.hpp"

using std::string;

class Manager {
public:
	Manager(bool override_instance = false);
	~Manager();

	void AddAluno(string nome, string codigo, Aluno::Nivel, Pessoa::Sexo);
	void PrintAlunos();

	static Manager* GetInstance();
	static void SetInstance(Manager*);

private:
	static Manager* instance;
	std::vector<Aluno> alunos;
};
