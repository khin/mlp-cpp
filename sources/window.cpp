#include "window.hpp"
#include "aluno.hpp"
#include "manager.hpp"

#include <iostream>

using namespace std;

Window::Window(string glade){
	builder = Gtk::Builder::create_from_file(glade);

	if (!builder){
		cerr << "Failed to load file " << glade << endl;
		return;
	}

	builder->get_widget("appwindow", window);

	Gtk::Button* b;
	builder->get_widget("button_add", b);
	b->signal_clicked().connect(sigc::mem_fun(*this, &Window::on_add_click));
}

Window::~Window(){
	delete window;

	Manager::GetInstance()->PrintAlunos();
	cout << endl << "Bye now" << endl;
}

Gtk::Window* Window::get_window(){
	return window;
}

void Window::on_add_click(){
	string nome;
	string codigo;
	Pessoa::Sexo sexo = Pessoa::Sexo::INDEFINIDO;
	Aluno::Nivel nivel = Aluno::Nivel::INDEFINIDO;

	string buffer;
	Gtk::Entry* entry;
	Gtk::ComboBox* combo;

	builder->get_widget("entry_nome", entry);
	nome = entry->get_text();

	builder->get_widget("entry_codigo", entry);
	codigo = entry->get_text();

	builder->get_widget("combo_sexo", combo);
	buffer = combo->get_entry_text();

	if (buffer == "Masculino"){
		sexo = Pessoa::Sexo::MASCULINO;
	}
	else if (buffer == "Feminino"){
		sexo = Pessoa::Sexo::FEMININO;
	}

	builder->get_widget("combo_nivel", combo);
	buffer = combo->get_entry_text();

	if (buffer == "Graduação"){
		nivel = Aluno::Nivel::GRADUACAO;
	}
	else if (buffer == "Especialização"){
		nivel = Aluno::Nivel::ESPECIALIZACAO;
	}
	else if (buffer == "Mestrado"){
		nivel = Aluno::Nivel::MESTRADO;
	}
	else if (buffer == "Doutorado"){
		nivel = Aluno::Nivel::DOUTORADO;
	}

	Manager::GetInstance()->AddAluno(nome, codigo, nivel, sexo);
	cout << "Novo aluno adicionado" << endl;
}
