#pragma once

// pessoa.hpp

#include <string>
#include <ctime>
#include <chrono>
#include <iostream>

#include "comparable.hpp"

using std::chrono::system_clock;
using std::string;

class Pessoa : public Comparable {
public:
	enum Sexo { INDEFINIDO, MASCULINO, FEMININO };

	Pessoa();
	Pessoa(string nome, system_clock::time_point nascimento = system_clock::now(), Sexo sexo = INDEFINIDO);
	Pessoa(const Pessoa&);
	virtual ~Pessoa();

	Pessoa& operator=(const Pessoa&);

	string get_nome() const;
	bool set_nome(string);

	system_clock::time_point get_nascimento() const;
	bool set_nascimento(system_clock::time_point);

	Sexo get_sexo() const;
	bool set_sexo(Sexo);

	virtual string to_string() const;

	int compares_to(const Comparable& other) override;

private:
	string nome;
	system_clock::time_point nascimento;
	Sexo sexo;
};
