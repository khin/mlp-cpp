#include <typeinfo>
#include <iostream>
#include <vector>
#include <algorithm>

#include "aluno.hpp"
#include "catp.hpp"

using std::cout;
using std::endl;
using std::vector;

void catp_13_1_2(){
	Pessoa p1;
	Pessoa p2("Fulano", system_clock::now(), Pessoa::MASCULINO);
	Pessoa p3("1nv4l1d0", system_clock::now(), (Pessoa::Sexo)5);

	cout << endl;
	cout << p1.to_string();
	cout << p2.to_string();
	cout << p3.to_string();
	cout << endl;

	Pessoa p4 = p1;
	Pessoa p5;

	p5 = p2;

	cout << endl;
	cout << p4.to_string();
	cout << p5.to_string();
	cout << endl;

	Aluno a1("Fulano", "00000001", Aluno::GRADUACAO, system_clock::now(), Pessoa::MASCULINO);
	Aluno a2;
	Aluno a3("Beltrano", "_WEI@#", (Aluno::Nivel)9, system_clock::now(), Pessoa::FEMININO);

	cout << endl;
	cout << a1.to_string();
	cout << a2.to_string();
	cout << a3.to_string();
	cout << endl;
}

void catp_13_2_1(){
	cout << endl << "---------- Exercício 1 ----------" << endl;

	Pessoa *p1 = new Pessoa("Ana Paula", system_clock::now(), Pessoa::FEMININO);
	Pessoa *p2 = new Aluno("Jose Silva", "01001010", Aluno::GRADUACAO, system_clock::now(), Pessoa::FEMININO);

	cout << p1->to_string();
	cout << p2->to_string(); // C++14
}

void catp_13_2_2(){
	cout << endl << "---------- Exercício 2 ----------" << endl;

	vector<Pessoa> vetor;
	int i=0;

	// verifica tamanho atual (original)
	cout << "Tamanho do vetor = " << vetor.size() << endl;

	// coloca alguns numeros no vetor
	cout << "Inserindo alguns elementos..." << endl;
	for(i = 0; i < SIZE; i++){
		vetor.push_back(Pessoa()); // adiciona no final do vetor
	}

	// verifica tamanho atual
	cout << "Tamanho do vetor = " << vetor.size() << endl;

	// mostrando elementos adicionados
	for(i = 0; i < SIZE; i++){
		cout << "vetor[" << i << "] = " << vetor[i].to_string() << endl;
	}

	// usando iterador para acessar os elementos do vetor
	vector<Pessoa>::iterator elemento = vetor.begin();
	i=0;
	while( elemento != vetor.end()) {
		cout << " elemento " << i++ << " = " << elemento->to_string() << endl;
		elemento++;
	}
}

void print(Pessoa& p){
		try {
			Aluno& a = dynamic_cast<Aluno&>(p);
			cout << a.to_string();
		}
		catch (std::bad_cast& bc) {
			cout << p.to_string();
		}
	}

void catp_13_2_3(){
	cout << endl << "---------- Exercício 3 ----------" << endl;

	vector<Pessoa*> pessoas(SIZE);

    for (int i = 0; i < SIZE; i++){
        if (i % 3 == 0){
            pessoas[i] = new Aluno();
        }
        else {
            pessoas[i] = new Pessoa();
        }
    }

	for (Pessoa *p : pessoas){
		print(*p); // C++11
		//cout << p->to_string(); // C++14
	}
}

bool compare(Comparable *a, Comparable *b){
	return (a->compares_to(*b) < 0);
}

template< typename T >
void sort(vector<T*>& v){
	std::sort(v.begin(), v.end(), compare);
}

void catp_13_2_4(){
	cout << endl << "---------- Exercício 4 ----------" << endl;

	vector<Pessoa*> pessoas(SIZE);

	pessoas[0] = new Pessoa("Z");
	pessoas[1] = new Pessoa("F");
	pessoas[2] = new Pessoa("L");
	pessoas[3] = new Pessoa("S");
	pessoas[4] = new Pessoa("G");
	pessoas[5] = new Pessoa("R");
	pessoas[6] = new Pessoa("T");
	pessoas[7] = new Pessoa("H");
	pessoas[8] = new Pessoa("A");
	pessoas[9] = new Pessoa("O");

	sort(pessoas);

	for (Pessoa *p : pessoas){
		cout << p->to_string();
	}

	for (int i = 0; i < SIZE; i++){
		delete pessoas[i];
	}
}
