// pessoa.cpp

#include <typeinfo>
#include <ctime>
#include <chrono>
#include "pessoa.hpp"

using namespace std;
using std::chrono::system_clock;

Pessoa::Pessoa(){
	set_nome("indefinido");
	set_nascimento(system_clock::now());
	set_sexo(Pessoa::INDEFINIDO);
}

Pessoa::Pessoa(const Pessoa& p) : Pessoa::Pessoa() {
	set_nome(p.get_nome());
	set_nascimento(p.get_nascimento());
	set_sexo(p.get_sexo());
}

Pessoa::Pessoa(string n, system_clock::time_point nasc, Sexo s) : Pessoa::Pessoa() {
	set_nome(n);
	set_nascimento(nasc);
	set_sexo(s);
}

Pessoa::~Pessoa(){ }

Pessoa& Pessoa::operator=(const Pessoa& p){
	set_nome(p.get_nome());
	set_nascimento(p.get_nascimento());
	set_sexo(p.get_sexo());

	return *this;
}

string Pessoa::get_nome() const{
	return nome;
}

bool Pessoa::set_nome(string n){
	bool success = false;

	success = !n.empty() and
		((n[0] >= 'A' and n[0] <= 'Z') or
		(n[0] >= 'a' and n[0] <= 'z'));

	if (success)
		nome = n;

	return success;
}

system_clock::time_point Pessoa::get_nascimento() const{
	return nascimento;
}

bool Pessoa::set_nascimento(system_clock::time_point data){
	bool success = false;

	success = data < system_clock::now();

	if (success)
		nascimento = data;

	return success;
}

Pessoa::Sexo Pessoa::get_sexo() const{
	return sexo;
}

bool Pessoa::set_sexo(Sexo s){
	bool success = ((int)s >= 0 and (int)s <= FEMININO);

	if (success)
		sexo = s;

	return success;
}

int Pessoa::compares_to(const Comparable& other){
	return get_nome().compare(dynamic_cast<const Pessoa&>(other).get_nome());
}

string Pessoa::to_string() const{
	system_clock::time_point tp = get_nascimento();
	time_t data = system_clock::to_time_t(tp);

	string sexos[] = { "indefinido", "masculino", "feminino" };

	string s(
		get_nome() +
		", sexo " + sexos[get_sexo()] +
		", " + asctime(localtime(&data))
	);

	return s;
}
