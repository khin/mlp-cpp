#pragma once

#include <string>
#include <gtkmm.h>

#define GLADE_FILE "ui.glade"

class Window {
public:
	Window(std::string glade_filename = GLADE_FILE);
	~Window();

	Gtk::Window* get_window();

protected:
	void on_add_click();

	Glib::RefPtr<Gtk::Builder> builder;
	Gtk::Window* window = nullptr;
};
