#pragma once

class Comparable{
public:

	virtual int compares_to(const Comparable& other) = 0;
};
