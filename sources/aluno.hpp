#pragma once

// aluno.hpp

#include "pessoa.hpp"

using std::string;

class Aluno : public Pessoa {
public:
	enum Nivel { INDEFINIDO, GRADUACAO, ESPECIALIZACAO, MESTRADO, DOUTORADO };

	Aluno();
	Aluno(const Aluno&);
	Aluno(string nome, string codigo = "00000000", Nivel n = Aluno::INDEFINIDO,
		system_clock::time_point nascimento = system_clock::now(), Sexo s = Pessoa::INDEFINIDO);
	~Aluno();

	Aluno& operator=(const Aluno&);

	string get_codigo() const;
	bool set_codigo(string);

	Nivel get_nivel() const;
	bool set_nivel(Nivel);

	string to_string() const override;

private:
	string codigo;
	Nivel nivel;
};
