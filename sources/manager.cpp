#include "manager.hpp"

using namespace std;

Manager* Manager::instance = nullptr;

Manager::Manager(bool override_instance){
	if (override_instance){
		SetInstance(this);
	}
}

Manager::~Manager(){ }

Manager* Manager::GetInstance(){
	if (!instance){
		instance = new Manager();
	}

	return instance;
}

void Manager::SetInstance(Manager* manager){
	if (manager){
		instance = manager;
	}
}

void Manager::AddAluno(string nome, string cod, Aluno::Nivel n, Pessoa::Sexo s){
	alunos.push_back(Aluno(nome, cod, n, system_clock::now(), s));
}

void Manager::PrintAlunos(){
	for (Aluno a : alunos){
		cout << a.to_string();
	}
}
