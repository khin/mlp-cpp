// aluno.cpp

#include "aluno.hpp"

using namespace std;

Aluno::Aluno() : Pessoa() {
	set_codigo("00000000");
	set_nivel(INDEFINIDO);
}

Aluno::Aluno(const Aluno& o) : Pessoa(o){
	set_codigo(o.get_codigo());
	set_nivel(o.get_nivel());
}

Aluno::Aluno(string nome, string cod, Nivel n, system_clock::time_point data, Sexo s)
: Pessoa(nome, data, s){
	set_codigo("00000000");
	set_nivel(INDEFINIDO);

	set_codigo(cod);
	set_nivel(n);
}

Aluno::~Aluno(){ }

Aluno& Aluno::operator=(const Aluno& o){
	set_nome(o.get_nome());
	set_nascimento(o.get_nascimento());
	set_sexo(o.get_sexo());

	set_codigo(o.get_codigo());
	set_nivel(o.get_nivel());

	return *this;
}

string Aluno::get_codigo() const{
	return codigo;
}

bool Aluno::set_codigo(string cod){
	bool success = not cod.empty();

	for (auto it = cod.begin(); success and it != cod.end(); ++it){
		success = *it >= '0' and *it <= '9';
	}

	if (success)
		codigo = cod;

	return success;
}

Aluno::Nivel Aluno::get_nivel() const{
	return nivel;
}

bool Aluno::set_nivel(Nivel n){
	bool success = ((int)n >= 0 and (int)n <= DOUTORADO);

	if (success)
		nivel = n;

	return success;
}

string Aluno::to_string() const{
	string sexos[] = { "indefinido", "masculino", "feminino" };
	string niveis[] = { "indefinido", "graduação",
		"especialização", "mestrado", "doutorado" };

	system_clock::time_point tp = get_nascimento();
	time_t data = system_clock::to_time_t(tp);

	string s(
		(get_nome() +
		", " + get_codigo() +
		", " + niveis[get_nivel()] +
		", sexo " + sexos[get_sexo()] +
		", " + asctime(localtime(&data)))
	);

	return s;
}
